package networkmanager

import "github.com/godbus/dbus/v5"

const (
	Interface = "org.freedesktop.NetworkManager"
	ObjectPath = "/org/freedesktop/NetworkManager"

	PropertyActiveConnections = Interface + ".ActiveConnections"
	DeactivateConnection = Interface + ".DeactivateConnection"
	PropertyWirelessEnabled = Interface + ".WirelessEnabled"
	GetDevices = Interface + ".GetDevices"
	AddAndActivateConnection = Interface + ".AddAndActivateConnection"
)

func NewNetworkManager() (NetworkManager, error){
	var nm networkManager
	return &nm, nm.init(Interface, ObjectPath)
}

type NetworkManager interface {
	SetPropertyWirelessEnabled(bool) error
	GetPropertyActiveConnections() ([]ActiveConnection, error)
	DeactivateConnection(connection ActiveConnection) error
	GetPropertyWirelessEnabled() (bool, error)
	GetDeviceWireless() (DeviceWireless, error)
	AddAndActivateWirelessConnection(connection map[string]map[string]interface{}, device DeviceWireless, accessPoint AccessPoint) (ActiveConnection, error)
}

type networkManager struct {
	dbusBase
}

func (nm *networkManager) SetPropertyWirelessEnabled(state bool) error {
	return nm.setProperty(PropertyWirelessEnabled, state)
}

func (nm *networkManager) GetPropertyWirelessEnabled() (bool, error)  {
	return nm.getBoolProperty(PropertyWirelessEnabled)
}

func (nm *networkManager) GetPropertyActiveConnections() ([]ActiveConnection, error) {
	acPaths, err := nm.getSliceObjectProperty(PropertyActiveConnections)
	if err != nil {
		return nil, err
	}

	ac := make([]ActiveConnection, len(acPaths))
	for i, path := range acPaths {
		ac[i], err = NewActiveConnection(path)
		if err != nil {
			return ac, err
		}
	}

	return ac, nil
}

func (nm *networkManager) DeactivateConnection(c ActiveConnection) error {
	return nm.call(DeactivateConnection, c.GetPath())
}

func (nm *networkManager) GetDeviceWireless() (device DeviceWireless, err error) {
	var devicePaths []dbus.ObjectPath
	err = nm.callWithReturn(&devicePaths, GetDevices)
	if err != nil {
		return
	}

	for _, path := range devicePaths {
		device, err = DeviceFactory(path)
		if device != nil {
			return
		}
	}

	return nil, nil
}

func (nm *networkManager) AddAndActivateWirelessConnection(connection map[string]map[string]interface{}, device DeviceWireless, accessPoint AccessPoint) (ac ActiveConnection, err error) {
	var path1 dbus.ObjectPath
	var path2 dbus.ObjectPath

	err = nm.callWithReturn2(&path1, &path2, AddAndActivateConnection, connection, device.GetPath(), accessPoint.GetPath())
	if err != nil {
		return
	}

	ac, err = NewActiveConnection(path2)
	if err != nil {
		return
	}

	return
}