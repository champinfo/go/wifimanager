package networkmanager

import "github.com/godbus/dbus/v5"

const (
	// device
	DeviceInterface = Interface + ".Device"
	DevicePropertyDeviceType = DeviceInterface + ".DeviceType"

	// device wireless
	DeviceWirelessInterface = DeviceInterface + ".Wireless"
	DeviceWirelessPropertyAccessPoints = DeviceWirelessInterface + ".AccessPoints"
	DeviceWirelessRequestScan = DeviceWirelessInterface + ".RequestScan"
	DeviceWirelessPropertyLastScan = DeviceWirelessInterface + ".LastScan"
)

func DeviceFactory(objPath dbus.ObjectPath) (DeviceWireless, error) {
	d, err := NewDevice(objPath)
	if err != nil {
		return nil, err
	}

	deviceType, err := d.GetPropertyDeviceType()
	if err != nil {
		return nil, err
	}

	if deviceType == NmDeviceTypeWifi {
		return d, nil
	} else {
		return nil, err
	}
}

func NewDevice(objPath dbus.ObjectPath) (DeviceWireless, error){
	var d deviceWireless
	return &d, d.init(Interface, objPath)
}

type DeviceWireless interface {
	GetPath() dbus.ObjectPath
	GetPropertyDeviceType() (NmDeviceType, error)

	GetPropertyAccessPoints() ([]AccessPoint, error)
	RequestScan() error
	GetPropertyLastScan() (int64, error)
}

type deviceWireless struct {
	dbusBase
}

func (d *deviceWireless) GetPath() dbus.ObjectPath {
	return d.obj.Path()
}

func (d *deviceWireless) GetPropertyDeviceType() (NmDeviceType, error) {
	v, err := d.getUint32Property(DevicePropertyDeviceType)
	return NmDeviceType(v), err
}

func (d *deviceWireless) GetPropertyAccessPoints() ([]AccessPoint, error) {
	apPaths, err := d.getSliceObjectProperty(DeviceWirelessPropertyAccessPoints)
	if err != nil {
		return nil, err
	}

	ap := make([]AccessPoint, len(apPaths))
	for i, path := range apPaths {
		ap[i], err = NewAccessPoint(path)
		if err != nil {
			return ap, err
		}
	}

	return ap, nil
}

func (d *deviceWireless) RequestScan() error {
	var options map[string]interface{}
	return d.obj.Call(DeviceWirelessRequestScan, 0, options).Store()
}

func (d *deviceWireless) GetPropertyLastScan() (int64, error) {
	return d.getInt64Property(DeviceWirelessPropertyLastScan)
}