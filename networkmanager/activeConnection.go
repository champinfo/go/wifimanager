package networkmanager

import "github.com/godbus/dbus/v5"

const (
	ActiveConnectionInterface = Interface + ".Connection.Active"
	ActiveConnectionPropertyType = ActiveConnectionInterface + ".Type"
	ActiveConnectionPropertySpecificObject = ActiveConnectionInterface + ".SpecificObject"
)

type ActiveConnection interface {
	GetPath() dbus.ObjectPath
	GetPropertyType() (string, error)
	GetPropertySpecificObject() (AccessPoint, error)
}

type activeConnection struct {
	dbusBase
}

func NewActiveConnection(objPath dbus.ObjectPath) (ActiveConnection, error) {
	var a activeConnection
	return &a, a.init(Interface, objPath)
}

func (a *activeConnection) GetPath() dbus.ObjectPath {
	return a.obj.Path()
}

func (a *activeConnection) GetPropertyType() (string, error) {
	return a.getStringProperty(ActiveConnectionPropertyType)
}

func (a *activeConnection) GetPropertySpecificObject() (AccessPoint, error) {
	path, err := a.getObjectProperty(ActiveConnectionPropertySpecificObject)
	if err != nil {
		return nil, err
	}
	ap, err := NewAccessPoint(path)
	if err != nil {
		return nil, err
	}
	return ap, nil
}
