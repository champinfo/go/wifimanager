package networkmanager

import "github.com/godbus/dbus/v5"

const (
	SettingsInterface = Interface + ".Settings"
	SettingsObjectPath = ObjectPath + "/Settings"
	SettingsListConnections = SettingsInterface + ".ListConnections"
)

type Settings interface {
	ListConnections() ([]Connection, error)
}

func NewSettings() (Settings, error) {
	var s settings
	return &s, s.init(Interface, SettingsObjectPath)
}

type settings struct {
	dbusBase
}

func (s *settings) ListConnections() ([]Connection, error){
	var connectionPaths []dbus.ObjectPath

	err := s.callWithReturn(&connectionPaths, SettingsListConnections)

	if err != nil {
		return nil, err
	}

	connections := make([]Connection, len(connectionPaths))

	for i, path := range connectionPaths {
		connections[i], err = NewConnection(path)
		if err != nil {
			return connections, err
		}
	}

	return connections, nil
}
