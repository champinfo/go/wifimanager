package networkmanager

import "github.com/godbus/dbus/v5"

const (
	ConnectionInterface = SettingsInterface + ".Connection"

	ConnectionDelete = ConnectionInterface + ".Delete"
	ConnectionPropertyFilename = ConnectionInterface + ".Filename"
)

type Connection interface {
	Delete() error
	GetPropertyFilename() (string, error)
}

func NewConnection(objPath dbus.ObjectPath) (Connection, error) {
	var c connection

	return &c, c.init(Interface, objPath)
}

type connection struct {
	dbusBase
}

func (c *connection) Delete() error {
	return c.call(ConnectionDelete)
}

func (c *connection) GetPropertyFilename() (string, error)  {
	return c.getStringProperty(ConnectionPropertyFilename)
}