package networkmanager

type NmDeviceType uint32

const (
	NmDeviceTypeUnknown      NmDeviceType = 0  // unknown device
	NmDeviceTypeGeneric      NmDeviceType = 14 // generic support for unrecognized device types
	NmDeviceTypeEthernet     NmDeviceType = 1  // a wired ethernet device
	NmDeviceTypeWifi         NmDeviceType = 2  // an 802.11 Wi-Fi device
	NmDeviceTypeUnused1      NmDeviceType = 3  // not used
	NmDeviceTypeUnused2      NmDeviceType = 4  // not used
	NmDeviceTypeBt           NmDeviceType = 5  // a Bluetooth device supporting PAN or DUN access protocols
	NmDeviceTypeOlpcMesh     NmDeviceType = 6  // an OLPC XO mesh networking device
	NmDeviceTypeWimax        NmDeviceType = 7  // an 802.16e Mobile WiMAX broadband device
	NmDeviceTypeModem        NmDeviceType = 8  // a modem supporting analog telephone, CDMA/EVDO, GSM/UMTS, or LTE network access protocols
	NmDeviceTypeInfiniband   NmDeviceType = 9  // an IP-over-InfiniBand device
	NmDeviceTypeBond         NmDeviceType = 10 // a bond master interface
	NmDeviceTypeVlan         NmDeviceType = 11 // an 802.1Q VLAN interface
	NmDeviceTypeAdsl         NmDeviceType = 12 // ADSL modem
	NmDeviceTypeBridge       NmDeviceType = 13 // a bridge master interface
	NmDeviceTypeTeam         NmDeviceType = 15 // a team master interface
	NmDeviceTypeTun          NmDeviceType = 16 // a TUN or TAP interface
	NmDeviceTypeIpTunnel     NmDeviceType = 17 // a IP tunnel interface
	NmDeviceTypeMacvlan      NmDeviceType = 18 // a MACVLAN interface
	NmDeviceTypeVxlan        NmDeviceType = 19 // a VXLAN interface
	NmDeviceTypeVeth         NmDeviceType = 20 // a VETH interface
	NmDeviceTypeMacsec       NmDeviceType = 21 // a MACsec interface
	NmDeviceTypeDummy        NmDeviceType = 22 // a dummy interface
	NmDeviceTypePpp          NmDeviceType = 23 // a PPP interface
	NmDeviceTypeOvsInterface NmDeviceType = 24 // a Open vSwitch interface
	NmDeviceTypeOvsPort      NmDeviceType = 25 // a Open vSwitch port
	NmDeviceTypeOvsBridge    NmDeviceType = 26 // a Open vSwitch bridge
	NmDeviceTypeWpan         NmDeviceType = 27 // a IEEE 802.15.4 (WPAN) MAC Layer Device
	NmDeviceType6lowpan      NmDeviceType = 28 // 6LoWPAN interface
	NmDeviceTypeWireguard    NmDeviceType = 29 // a WireGuard interface
	NmDeviceTypeWifiP2p      NmDeviceType = 30 // an 802.11 Wi-Fi P2P device (Since: 1.16)
)
