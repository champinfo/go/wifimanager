package networkmanager

import "github.com/godbus/dbus/v5"

const (
	AccessPointInterface = Interface + ".AccessPoint"
	AccessPointPropertySsid = AccessPointInterface + ".Ssid"
)

type AccessPoint interface {
	GetPath() dbus.ObjectPath
	GetPropertySSID() (string, error)
}

type accessPoint struct {
	dbusBase
}

func NewAccessPoint(objectPath dbus.ObjectPath) (AccessPoint, error)  {
	var a accessPoint
	return &a, a.init(Interface, objectPath)
}

func (a *accessPoint) GetPath() dbus.ObjectPath {
	return a.obj.Path()
}

func (a *accessPoint) GetPropertySSID() (string, error) {
	r, err := a.getSliceByteProperty(AccessPointPropertySsid)
	if err != nil {
		return "", nil
	}
	return string(r), nil
}

