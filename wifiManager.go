package wifimanager

import (
	"errors"
	"fmt"
	"gitlab.com/champinfo/go/wifimanager/networkmanager"
	"log"
	"net"
	"strings"
	"time"
)

//var instance *WifiManager
//var once sync.Once

func NewWifiManager() *WifiManager {
	//once.Do(func() {
	//	instance := WifiManager{
	//		wirelessDevice: nil,
	//		networkMgr: nil,
	//	}
	//	netMgr, _ := networkmanager.NewNetworkManager()
	//	instance.networkMgr = netMgr
	//})
	//return instance
	wifi := WifiManager{
		wirelessDevice: nil,
	}
	netMgr, _ := networkmanager.NewNetworkManager()
	wifi.networkMgr = netMgr
	return &wifi
}

type WifiManager struct {
	networkMgr     networkmanager.NetworkManager
	wirelessDevice networkmanager.DeviceWireless
}

func (w *WifiManager) IsConnect() bool {
	host := "google.com"
	port := "80"
	conn, err := net.DialTimeout("tcp", host+":"+port, time.Second)
	if err != nil {
		log.Printf("[wifiManager-IsConnect]#%v", err)
		return false
	}
	defer func() {
		if err := conn.Close(); err != nil {
			log.Println("[wifiManager-IsConnect] close connection error ", err)
		}
	}()
	return true
}

func (w *WifiManager) ScanWifi() error {
	device, err := w.networkMgr.GetDeviceWireless()
	if err != nil {
		log.Println("[wifiManager-ScanWifi] err ", err.Error())
		return err
	}
	w.wirelessDevice = device
	if err := w.scan(); err != nil {
		log.Println("[wifiManager-ScanWifi] err ", err.Error())
		return err
	}
	return nil
}

func (w *WifiManager) GetWifiList() []string {
	var ssids []string
	aps, _ := w.wirelessDevice.GetPropertyAccessPoints()
	for _, ap := range aps {
		ssid, _ := ap.GetPropertySSID()
		ssids = append(ssids, ssid)
	}
	return ssids
}

func (w *WifiManager) Connect(ssid, password string) error {
	ap, err := w.findTargetAccessPoint(ssid)
	if err != nil {
		log.Println("[wifiManager-Connect] err ", err.Error())
		return err
	}
	err = w.deleteStoredConnectionData(ssid)
	if err != nil {
		log.Println("[wifiManager-Connect] err ", err.Error())
		return err
	}

	err = w.connect(ap, password)
	if err != nil {
		log.Println("[wifiManager-Connect] err ", err.Error())
		return err
	}
	return nil
}

func (w *WifiManager) deleteStoredConnectionData(ssid string) error {
	settings, err := networkmanager.NewSettings()
	if err != nil {
		log.Println("[wifiManager-deleteStoredConnectionData] err ", err.Error())
		return err
	}

	conns, err := settings.ListConnections()
	if err != nil {
		log.Println("[wifiManager-deleteStoredConnectionData] err ", err.Error())
		return err
	}

	for _, conn := range conns {
		filename, _ := conn.GetPropertyFilename()
		if strings.Contains(filename, ssid) {
			if err := conn.Delete(); err != nil {
				log.Println("[wifiManager-deleteStoredConnectionData] delete connection error ", err.Error())
			}
		}
	}
	return nil
}

func (w *WifiManager) scan() error {
	return w.wirelessDevice.RequestScan()
}

func (w *WifiManager) findTargetAccessPoint(target string) (networkmanager.AccessPoint, error) {
	aps, _ := w.wirelessDevice.GetPropertyAccessPoints()
	for _, ap := range aps {
		ssid, _ := ap.GetPropertySSID()
		if ssid == target {
			return ap, nil
		}
	}
	return nil, errors.New(fmt.Sprintf("[wifiManager-deleteStoredConnectionData] can not find target access point %s", target))
}

// connect 連線至該 wifi
func (w *WifiManager) connect(ap networkmanager.AccessPoint, password string) error {
	connection := make(map[string]map[string]interface{})
	connection["802-11-wireless"] = make(map[string]interface{})
	connection["802-11-wireless"]["security"] = "802-11-wireless-security"
	connection["802-11-wireless-security"] = make(map[string]interface{})
	connection["802-11-wireless-security"]["key-mgmt"] = "wpa-psk"
	connection["802-11-wireless-security"]["psk"] = password
	_, err := w.networkMgr.AddAndActivateWirelessConnection(connection, w.wirelessDevice, ap)
	if err != nil {
		log.Println("[wifiManager-connect] err ", err.Error())
	}
	return err
}

func (w *WifiManager) enableNDisableWifi(enable bool) bool {
	err := w.networkMgr.SetPropertyWirelessEnabled(enable)
	if err != nil {
		log.Println("[wifiManager-enableNDisableWifi] err ", err.Error())
	}
	return err == nil
}
